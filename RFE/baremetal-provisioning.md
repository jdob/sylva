# Enhancement/Feature description

The Sylva vision is to support various use-cases, including some networking functions (such as RAN, UPF) that need to run their containerized workloads directly on bare metal for performance reasons.

The Sylva stack should be able to be deployed on a baremetal environment, where either the management cluster or the workload clusters, or both, are going to be hosted on baremetal servers, with no virtualization layers.

This RFE is being targetted for the Sylva 0.2 release.

# Technical implementation

The Sylva project has chosen the [Metal3](http://metal3.io/) infrastructure provider to handle the API definition and underlying tech to facilitate the actual bare metal provisioning. More technical details are given in [Metal3 in Sylva]( https://gitlab.com/sylva-projects/sylva-core/-/blob/main/docs/metal3.md).

## Implementation overview

There are three container images needed for the Metal3 deployment:
* OpenStack Ironic - Performs the actual bare metal communication and provisioning
* OpenStack Ironic IPA Downloader - Used by Ironic on bare metal machines to facilitate control
* MariaDB - SQL database for Ironic

As the Metal3 project does not provide images for consumption, the main objective of this RFE is to integrate container images that are production ready and come from a software supply chain that will enable security certifications and compliance with regulatory requirements going forward. These images can currently be found at [1], but that may change in the near future.

When deployed in a cluster, the Metal3 suite of artifacts includes:
* Baremetal Operator
* Ironic
* MariaDB

Deployment of these components will be done through Helm charts. The Sylva installation process will use the Helm charts hosted and maintained at [2] (as of May 24, the specific charts have not yet been migrated to that repo and can currently be found at [3]).

[1] https://build.opensuse.org/project/show/isv:metal3:BCI:ironic
[2] https://github.com/suse-edge/charts
[3] https://github.com/rancher-sandbox/baremetal/

## Work checklist

* Add an entry under `environment-values` similar to existing CAPI variants (CAPO, CAPV) for ease in creating a CAPM3 environment.
* Add documentation to the project root README for CAPM3 under "Process to be followed for various infrastructure provider"
* Adjust the installation to pull the Helm charts from the `suse-edge` repo instead of `sylva-elements`
* Adjust variables previously passed into the `sylva-elements` charts to their `suse-edge` counterparts

## Alternatives

There is some existing work towards Metal3 in Sylva, which presented a few options:
* Use the existing charts under `sylva-elements` and change them to point to the SUSE-built images
* Fork the SUSE charts in `suse-edge` into Sylva (presumably `sylva-elements`) and use those

Given SUSE's ongoing commitment to Metal3 development, the suggestion is to use the charts directly from the `suse-edge` repo. If this becomes a problem in the future, a fork can be created directly into Sylva. But it's much easier to identify and react to that situation than it is to revert from a fork to an external repo as the source of truth.

# How to test it

Automated testing for bare metal hardware is obviously complicated due to the need for extra hardware. SUSE is in the process of building out public-facing CI for it's Metal3 images and Helm charts. The use of the `suse-edge` repo as the source of truth for the Helm charts allows that CI to serve these testing needs.